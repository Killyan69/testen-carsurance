## Testen
Voor deze module "Testen" heb ik Unit tests geschreven voor de "PremiumCalculation" klasse.
Dit heb ik gedaan op basis van de requirements. In dit bestand zal ik uitleggen waarom ik welke test heb gedaan,
waarom ik voor deze data-invulling heb gekozen en welke technieken ik heb gebruikt. Ook leg ik uit
welke fouten ik in de applicatie heb gevonden en hoe ik deze heb verbeterd.

## Mocks
Ik ben begonnen met het maken van mocks. Deze heb ik gemaakt voor de "Vehicle" en "PolicyHolder".

### Vehicle
Als eerste heb ik de gegevens van mijn motor gebruikt, die waren voor mij makkelijk te onthouden en leuk om mee te testen.
Hiervoor heb ik het vermogen (105kW), het bedrag waarvoor ik deze heb gekocht (�1700) en het bouwjaar (1999)  ingevuld.

###PolicyHolder
Als tweede mock heb ik de PolicyHolder ingevoerd. Hierbij heb ik gebruik gemaakt van gegevens die niet voor extra kosten
bovenop de premie zouden zorgen. Dit omdat het het testen een stuk makkelijker zou maken.
Een leeftijd van 26, een rijbewijs van 6 jaar oud, een postcode van 8231 en 0 Schadevrije jaren waren gebruikt.

Na het maken van de Policyholder mock heb ik nog even een BasisPremium neergezet, mede ter bevestiging van mijn eerste 
test.

##Tests

###Check if base premium calculation is correct [Fact]
Ik ben hier begonnen, omdat de accuraatheid van deze test belangrijk is voor de rest van de tests. 
Eerst keek ik naar de formule van de functie zelf. Hier stond een fout, dus die heb ik aangepast. In de code
zie je welke aanpassing ik heb gedaan. 
Nu de formule klopte, heb ik met mijn motorgegevens uitgerekend wat het premiebedrag zou moeten zijn. Dat was �5.53.

Vervolgens heb ik met de gegevens de functie uitgevoerd, en het bedrag dat er uit kwam was 5.333333334. Ik heb Math.Round
gebruikt om het op twee decimalen af te ronden.

### Check if premium is 15% higher < 23 years &  licenseAge <5 [Theory]
Ik zag meteen een fout in de functie. Er stond een licenseAge van =<5, maar dit moest alleen kleiner zijn dan 5.

Vervolgens heb ik mijn mocks ingezet en heb ik de WA-verzekering gebruikt. Dit omdat hier verder geen verhoogde premie
op zou zijn om het testen goed te kunnen testen.

Ik heb als actualOutcome een aparte variabele actualOutcomeDouble gemaakt om het resultaat van de functie af te ronden
op twee decimalen.

De datavariables die ik heb gebruikt zijn de age, licenseAge en een "double" expectation. De expectation is het getal
waarmee de premium vermenigvuldugd zou worden. Boven de functie onder de[Theory] heb ik deze variabelen veranderd
aan de hand van wat ik wilde testen. Eerst natuurlijk de leeftijden 22, 23 en 24. Bij 22 zou de verwachte uitkomst
15% hoger moeten uitvallen dan bij de leeftijden 23 en 24. Dit klopte.

Vervolgens heb ik de licenseAges 4, 5 en 6 gebruikt. Hier zou ook een rijbewijs die 4 jaar geleden is behaald een
verhoging moeten hebben van 15% en de 5 en 6 jaar niet. Dit klopte ook.

### Check if premium is higher at certain postal codes [Theory]
Ik heb meteen [Theory] values aangemaakt voor welke postcodes geen, 2% en 5% verhoging zouden krijgen. Vervolgens
heb ik hier weer de vermenigvuldiging gebruikt als "double" expectation.

Vervolgens weer de mockdata gebruikt. Wel bij elke entry natuurlijk de postcode anders in de [InlineData] met daarnaast
het verwachte vermenigvuldigingscijfer. Na de tests te runnen, klopte het. de 0, 2 en 5% verhogingen werken zoals bedoeld.

### Check if no-claim years above 6 get a discount [Theory]
Elk jaar beginnend vanaf het zesde jaar krijg je een korting van 5% op je premie. 

Ik heb weer gebruik gemaakt van de mocks, deze keer de noClaimYears van de PolicyHolder aangepast.
Ik heb hier de values 0 tm 19 gebruikt. Bij elke entry keek ik of het vermenigvuldingscijfer klopte. Bij 0 schadevrije
jaren was het vermenigvuldigingscijfer 1.00. Vanaf 6 jaar werd deze 0.95 en elk jaar opvolgend verlaagde 0.05, tot een
minimum van 0.35 om het kortingsbedrag van 65% vast te stellen. Ik heb zowel een marge gepakt van een twee jaar voor 
de 6 jaar en 1 jaar na, om te kijken of het kortingsbedrag nog klopte. Dit klopte vervolgens en de tests waren met succes
afgerond. 

### Check if different coverages increase the premium by the right amount [Fact]
Zoals altijd, de mocks gebruikt. Vervolgens berekend wat het bedrag van de WA_PLUS zou moeten zijn en die van de ALL_RISK.
De basispremie was natuurlijk al berekend bij de eerste test. 
Ik heb dus een test geschreven om te kijken of het bedrag inderdaad 5.33 * 1.2 zou zijn bij de WA_PLUS. Dit klopte.

Ook bij de ALL_RISK heb ik geschreven dat de premie 5.33 * 2 moet zijn. Ook dit klopte. Weer een test met succes 
afgerond.

### Check if 2.5% discount is calculated when paid yearly [Fact]
Wederom de mocks gebruikt (super handig, die mocks!). Met de hand berekend wat het juiste bedrag zou moeten zijn
als het jaar in ��n keer betaald werd. Nu heb ik het bedrag dat betaald is als je in ��n keer in een jaar zou betalen.
In de volgende test kijk ik wat het betaalde bedrag is per jaar als je maandelijks zou betalen.

### Check price when paid monthly [Fact]
Om dit nauwkeurig te testen om afrondingsverschillen te voorkomen, heb ik mijn vermogen van de motor verhoogd naar 1500.
105 kW was te weinig om accuraat het bedrag te kunnen testen. Ik had natuurlijk ook het aankoopbedrag kunnen veranderen.

Vervolgens heb ik berekend wat de premie zou moeten zijn voor het hele jaar als ik maandelijks zou betalen.

Als ik dit bedrag vergelijk met de vorige test over de jaarlijkse betaling, zien we dat deze inderdaad 2,5% lager uitvalt.

## Conclusie
Op basis van alle tests, kan ik ook bevestigen dat er voor het eindbedrag inderdaad ook wordt afgerond op twee decimalen.
Ik heb hier geen aparte test voor geschreven. 

## Stryker
Nadat alle tests waren geslaagd, heb ik Stryker gebruikt om te kijken hoe het er voor stond. Hier kwamen een aantal 
ongedekte mutanten, maar na even zoeken heb ik deze opgelost. Ik heb wel twee mutanten die nog leven in de PremiumCalculation,
maar ik kon zelf niet vinden hoe ik dit kon oplossen.

Ik vond het een leuke opdracht en super bedankt voor alle leerzame lessen! Alles was uitstekend uitgelegd en behandeld.
