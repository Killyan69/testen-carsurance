using Moq;
using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTests
    {
        // Mock Vehicle
        private static Mock<Vehicle> MockVehicle(int powerInKW = 105)
        {
            var mockVehicle = new Mock<Vehicle>(105, 1700, 1999);
            mockVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(powerInKW);
            mockVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(1700);
            mockVehicle.Setup(vehicle => vehicle.Age).Returns(22);
            return mockVehicle;
        }

        // Mock PolicyHolder
        private static Mock<PolicyHolder> MockPolicyHolder(int age = 28, int licenseAge = 10, int postalCode = 8231, int noClaimYears = 0)
        {
            var mockPolicyHolder = new Mock<PolicyHolder>(MockBehavior.Default, 26, "24-06-2015", 8231, 0);
            mockPolicyHolder.Setup(policyHolder => policyHolder.Age).Returns(age);
            mockPolicyHolder.Setup(policyHolder => policyHolder.LicenseAge).Returns(licenseAge);
            mockPolicyHolder.Setup(policyHolder => policyHolder.PostalCode).Returns(postalCode);
            mockPolicyHolder.Setup(policyHolder => policyHolder.NoClaimYears).Returns(noClaimYears);
            return mockPolicyHolder;
        }

        // Base premium number for tests based on my 1999 Kawasaki ZX-9R
        private static readonly double BasePremium = 5.3333333;



        [Fact]
        public void CheckIfCalculationBasePremiumIsCorrectTest()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(105, 1700, 1999);
            double expectedOutcome = 5.33;

            // Act
            double premiumCalculation = Math.Round(PremiumCalculation.CalculateBasePremium(vehicle),2);

            // Assert
            Assert.Equal(expectedOutcome, premiumCalculation);
        }

        [Theory]
        // Testing if age is below 23 - if so, 15% should be added
        [InlineData(22, 7, 1.15)]
        [InlineData(23, 7, 1.00)]
        [InlineData(24, 7, 1.00)]

        // Testing if driver license is below 5 years old - if so, 15% should be added
        [InlineData(25, 4, 1.15)]
        [InlineData(25, 5, 1.00)]
        [InlineData(25, 6, 1.00)]

        public void CheckIfPremiumIs15PercentHigherWhenAgeIsBelow23OrLicenseAgeIsUnder5YearsTest(int age, int licenseAge, double expectation)
        {
            // Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder(age, licenseAge).Object;

            // Act
            var actualOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var actualOutcomeDouble = Math.Round(actualOutcome.PremiumAmountPerYear, 2);
            var expectedMultiplier = Math.Round(expectation * BasePremium, 2);

            // Assert
            Assert.Equal(expectedMultiplier, actualOutcomeDouble);

        }

        [Theory]
        // Expecting 2% increase here
        [InlineData(999, 1.02)]
        [InlineData(3601, 1.02)]
        [InlineData(3600, 1.02)]
        [InlineData(4499, 1.02)]

        // Expecting 5% increase here
        [InlineData(1000, 1.05)]
        [InlineData(3599, 1.05)]

        // No increase here
        [InlineData(4501, 1.00)]
        [InlineData(8231, 1.00)]

        public void CheckIfPremiumIsHigherAtCertainPostalCodesTest(int postalCode, double expectation)
        {
            // Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder(postalCode : postalCode).Object;

            // Act
            var actualOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var actualOutcomeDouble = Math.Round(actualOutcome.PremiumAmountPerYear, 2);
            var expectedMultiplier = Math.Round(expectation * BasePremium, 2);

            // Assert
            Assert.Equal(expectedMultiplier, actualOutcomeDouble);
        }

        [Theory]
        [InlineData(0, 1.00)] // Expecting no discount
        [InlineData(1, 1.00)] // Expecting no discount
        [InlineData(5, 1.00)] // Expecting no discount
        [InlineData(6, 0.95)] // Expecting 5% discount
        [InlineData(7, 0.90)] // Expecting 10% discount
        [InlineData(8, 0.85)] // Expecting 15% discount
        [InlineData(9, 0.80)] // Expecting 20% discount
        [InlineData(10, 0.75)] // Expecting 25% discount
        [InlineData(11, 0.70)] // Expecting 30% discount
        [InlineData(12, 0.65)] // Expecting 35% discount
        [InlineData(13, 0.60)] // Expecting 40% discount
        [InlineData(14, 0.55)] // Expecting 45% discount
        [InlineData(15, 0.50)] // Expecting 50% discount
        [InlineData(16, 0.45)] // Expecting 55% discount
        [InlineData(17, 0.40)] // Expecting 60% discount
        [InlineData(18, 0.35)] // Expecting 65% discount
        [InlineData(19, 0.35)] // Expecting 65% discount


        public void CheckIfNoClaimYearsAbove6GetDiscountsBasedOnYearsOfNoClaimTest(int noClaimYears, double expectation)
        {
            // Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder(noClaimYears : noClaimYears).Object;

            // Act
            var actualOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var actualOutcomeDouble = Math.Round(actualOutcome.PremiumAmountPerYear, 2);
            var expectedDiscount = Math.Round(expectation * BasePremium, 2);

            // Assert
            Assert.Equal(expectedDiscount, actualOutcomeDouble);

        }

        [Fact]

        public void CheckIfDifferentCoveragesIncreaseByTheRightAmountsTest()
        {
            // Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder().Object;
            double expectedWA = 5.33;
            double expectedWA_PLUS = 6.4;  
            double expectedALL_RISK = 10.67;
            

            // Act
            var outcomeWA = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var roundedWA = Math.Round(outcomeWA.PremiumAmountPerYear, 2);
            var outcomeWA_PLUS = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA_PLUS);
            var roundedWA_PLUS = Math.Round(outcomeWA_PLUS.PremiumAmountPerYear, 2);
            var outcomeALL_RISK = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.ALL_RISK);
            var roundedALL_RISK = Math.Round(outcomeALL_RISK.PremiumAmountPerYear, 2);

            // Assert
            Assert.Equal(expectedWA, roundedWA);
            Assert.Equal(expectedWA_PLUS, roundedWA_PLUS);
            Assert.Equal(expectedALL_RISK, roundedALL_RISK);

        }

        [Fact]
        public void CheckTwoPointFivePercentDiscountWhenPaidByYearTest()
        {
            // Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder().Object;
            double expectedOutcomeYearly = 5;
            
            // Act
            var actualOutcomeYearly = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            // Assert
            Assert.Equal(expectedOutcomeYearly, Math.Round(actualOutcomeYearly * 1.025));
        }

        [Fact]

        public void CheckPriceWhenPaidMonthlyTest()
        {
            // Changed the vehicle power to 1500 to accurately test this method - 105 was too low to get accurate results
            // Arrange
            
            var mockVehicle = MockVehicle(1500).Object;
            var mockPolicyHolder = MockPolicyHolder().Object;
            double expectedOutcomeMonthly = 8.19;

            // Act

            var actualOutcomeMonthly = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            // Assert
            Assert.Equal(expectedOutcomeMonthly, actualOutcomeMonthly);

        }
    }

    
}
